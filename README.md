# php-alura-search-courses

This project was developed as part of a course at Alura to learn how to use Composer. It provides a simple and easy-to-use search functionality for Alura courses.

## Installation

To install the `alura-search-courses` package, you can use Composer. Run the following command in your terminal:

```
composer require olooeez/alura-search-courses
```

## Contributing

If you wish to contribute to this project, feel free to open a merge request. We welcome all forms of contribution!

## License

This project is licensed under the [MIT](https://gitlab.com/alura-courses-code/php/php-alura-search-courses/-/blob/main/LICENSE). Refer to the LICENSE file for more details.
